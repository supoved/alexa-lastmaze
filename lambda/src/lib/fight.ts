import { Monster, AttackType, Player, Mobile, MonsterInfo } from "./core/types";
import { monsters } from "./core/monsters";
import { isAttackStronger } from "./tools/atack";
import { pickRandomItem } from "./tools/random";
import { map, keys, chain } from "lodash";

const monsterHitMessages = ['You hit /enemy/', 'You damaged /enemy/', 'Nice blow!', 'Nice hit!', '/enemy/ is wounded', 'Hit!', '', 'Smack!', 'Right on target!'];
const playerHitMessages = ['Oh, no!', 'No, it hit you!'];
const monsterKilledMessages = ['And /enemy/ is dead now', 'And /enemy/ is defeated', 'And enemy is defeated'];

export interface AttackResult {
  success: boolean,
  message: string
}

export function generateMonster(level: number): Monster {
  let candidates = chain(keys(monsters)).map(key => { return monsters[key]; }).filter(monster => (<MonsterInfo>monster).level <= level).value();  
  return new Monster(pickRandomItem(candidates));
}

export function attackMonster(player: Player, monster: Monster, playerAttack: AttackType): AttackResult {
  let monsterAttack = getNextAttack();
  if (isAttackStronger(playerAttack, monsterAttack)) {
    applyDamage(monster, player.weapon.damage);

    return {
      success: true,
      message: `Your ${playerAttack.toLowerCase()} is stronger than enemy's ${monsterAttack.toLowerCase()}. `
        + pickRandomItem(monsterHitMessages).replace('/enemy/', monster.name) + ". "
        + (monster.isAlive ? '' : pickRandomItem(monsterKilledMessages).replace('/enemy/', monster.name))
    }
  }
  else if (isAttackStronger(monsterAttack, playerAttack)) {
    applyDamage(player, monster.damage);
    return {
      success: false,
      message: `Enemy ${monsterAttack.toLowerCase()} is stronger than your ${playerAttack.toLowerCase()}. `
        + pickRandomItem(playerHitMessages).replace('/enemy/', monster.name)
    };
  }

  return {
    success: false,
    message: `Enemy ${monsterAttack.toLowerCase()} is equal to your ${playerAttack.toLowerCase()}. `
  };
}

function applyDamage(victim: Mobile, damage: number) {
  victim.currentHealth -= damage;
  if (victim.currentHealth <= 0) {
    victim.isAlive = false;
  }
}

function getNextAttack(): AttackType {
  let attacks: AttackType[] = ['Rock', 'Paper', 'Scissors'];
  return pickRandomItem(attacks);
}