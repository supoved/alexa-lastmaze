export function removeRandomItem<T>(items: T[]): T {
  return items.splice(randomNumber(0, items.length - 1), 1)[0];
}

export function pickRandomItem<T>(items: T[]): T {
  return items[randomNumber(0, items.length - 1)];
}

export function randomNumber(from: number, to: number) {
  let result = Math.round(Math.random() * to) + from;
  console.log('randomNumber: ' + result);
  return result;
}

export function isSuccess(successChance: number): boolean {
  return Math.random() <= successChance + 0.01;
}