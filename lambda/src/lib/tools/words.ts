import { pickRandomItem, isSuccess } from './random';
import { keys, forEach, map } from 'lodash';

const navigation = {
  path_noway: ["There is no passage this way", "Can not go this way"],
  path_sealed: ["This path is sealed, try another."],
  path_monster: ["{monster} is blocking the way. Say 'scissors', 'rock' or 'paper' to atack or 'run away' to escape"],
  path_known: ["Looks like you have already been here.", "You have already been here."],
  path_new: ['You entered a new room.', 'The door reveals a new room.', '', 'New room.', '', '', 'Next maze room.'],
  path_choose: ['Choose where to go'],

  room_monster: ['{monster} is blocking the way. You have to fight or try to run.', '{monster} have blocked your way. You have to fight or try to run.', '{monster}. Fight or try to run.', 'There is {monster}. Fight or try to run.'],
  room_item: ['You have noticed {item}.', "There is {item} in the center of the room.", "In a dark coner you have noticed {item}."],
  room_view: ['It is dusty', 'It is dark', 'It is so dusty', 'It is dark here', 'It is all covered with spider web.', 'Old satues are standing on the sides of it.', 'It welcomes you with a light of a lonely tourch', "You can hear movenment in the dark corners of the room or it is just imagination.", "It is a long hall."],

  atack_choose: ["Choose your attack: rock, scissors or paper."],
  attack_next: ["Choose your next attack.", "Pick next.", "You pick?", "Pick your next attack", "You choose?"]
}

type DictionaryType = keyof typeof navigation;

const vowels = { 'a': true, 'e': true, 'i': true, 'o': true, 'u': true, 'y': true };

export function getArticle(word: string): string {
  let letter = word.trim()[0].toLowerCase();
  if (vowels[letter]) {
    return 'an';
  }
  return 'a';
}

export function getRandomMessage(type: DictionaryType, chance: number = 1, meta: any = undefined) {
  if (!isSuccess(chance)) return;

  let result = pickRandomItem(navigation[type]);

  if (meta) {
    forEach(keys(meta), (key) => { result = result.replace(`{${key}}`, meta[key]) });
  }

  return combine(result);
}

export function combine(...msg: string[]) {
  console.log('combine:' + JSON.stringify(msg));

  let result = '';
  for (let i = 0; i < msg.length; i++) {
    let statement = msg[i];
    if (!statement || statement == '') continue;
    statement.trim();

    if (statement[statement.length - 1] != '.') {
      statement += '.';
    }

    statement = statement[0].toUpperCase() + statement.substr(1);

    if (i != 0) {
      result += ' ';
    }

    result += statement;
  }

  return result.trim();
}