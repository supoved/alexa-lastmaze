import { AttackType } from "../core/types";

export function getOpositeAttackType(attackType: AttackType): AttackType {
  if (attackType === 'Rock') {
    return 'Paper';
  }
  if (attackType === 'Scissors') {
    return 'Rock';
  }

  if (attackType === 'Paper') {
    return 'Scissors';
  }

  if (attackType === 'Fire') {
    return 'Rock';
  }

  if (attackType === 'Water') {
    return 'Paper';
  }

  if (attackType === 'Electricity') {
    return 'Scissors';
  }
}

export function isAttackStronger(first: AttackType, second: AttackType) {
  if (first === 'Rock') {
    return second === 'Scissors' || second === 'Fire';
  } else if (first === 'Paper') {
    return second === 'Rock' || second === 'Water';
  } else if (first === 'Scissors') {
    return second === 'Paper' || second === 'Electricity';
  } else if (first === 'Fire') {
    return second === 'Paper' || second === 'Scissors';
  } else if (first === 'Water') {
    return second === 'Rock' || second === 'Electricity';
  } else if (first === 'Electricity') {
    return second === 'Paper' || second === 'Rock';
  }
  return false;
}