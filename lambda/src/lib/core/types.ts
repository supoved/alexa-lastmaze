export interface MazeState {
  player: Player,
  room: Room,
  knownRooms: Room[]
  stats: {
    roomsPassed: number,
    monstersDefeated: number,
    goldCollected: number
    //etc
  }
}

export interface Player extends Mobile {
  weapon: Weapon,
  inventory: Item[]
}

export interface Mobile {
  maxHealth: number,
  currentHealth: number,
  isAlive: boolean
}

export interface Room {
  id: number,
  exits: { [x: string]: Door }
  entrance: Door,
  content: RoomContent,
  monsters: Monster[],
  description: string
}

export interface Door {
  type: DoorType,
  content: DoorContent,
  roomId?: number
}

export type ExitType = "left" | "center" | "right";
export type DoorType = "OneWayDoor" | "Door" | "LockedDoor";
export type DoorContent = "Danger" | "Loot" | "Regular";

export type ItemType = "Healing" | "Weapon" | "Utility" | "Key" | "Treasure";
export type RoomContentType = "Chest";

export interface RoomContent {
  id: string,
  name: string,
  description: string,
  type: RoomContentType
}

export interface ContainerInfo extends RoomContent {
  lootLevel: number,
  lockLevel: number,
  health: number
}

export interface Container extends ContainerInfo {
  locked: boolean,
  items: Item[]
}

export interface Item {
  id: string,
  name: string,
  description: string,
  type: ItemType,
  level: number
}

export interface UsableItem extends Item {
  canBeUsedInBattle: boolean,
}

export interface Weapon extends UsableItem {
  damage: number
}

export interface Treasure extends Item {
  value: number
}

export type AttackType =
  "Rock" // beats scissors
  | "Scissors" //beats paper
  | "Paper" //beats rock
  | "Water" //beats rock, scissors
  | "Fire" //beats paper, scissors
  | "Electricity" //beats rock, paper

export interface MonsterInfo {
  id: string,
  name: string,
  health: number,
  damage: number,
  level: number,
  onDamageTakenText: string[];
  onDamageDoneText: string[];
}

export class Monster implements Mobile {
  id: string;
  name: string;
  maxHealth: number;
  damage: number;
  level: number;
  currentHealth: number;
  isAlive: boolean;
  onDamageTakenText: string[];
  onDamageDoneText: string[];

  public constructor(info: MonsterInfo) {
    this.id = info.id;
    this.name = info.name;
    this.maxHealth = info.health;
    this.damage = info.damage;
    this.level = info.level;
    this.onDamageTakenText = info.onDamageTakenText || [];
    this.onDamageDoneText = info.onDamageDoneText || [];

    this.currentHealth = this.maxHealth;
    this.isAlive = true;
  }
}
