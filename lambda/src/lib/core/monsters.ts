import { MonsterInfo } from "./types";

export const monsters = {
  rat: <MonsterInfo>{
    id: 'rat',
    name: 'a giant rat',
    level: 0,
    damage: 1,
    health: 1,
    onDamageDoneText: ['Squek', 'Squeek', 'Squeeek'],
    onDamageTakenText: ['Squek', 'Squeek', 'Squeeek'],
  },
  slime: <MonsterInfo>{
    id: 'slime',
    name: 'a slime',
    level: 0,
    damage: 1,
    health: 1,
    onDamageDoneText: [],
    onDamageTakenText: [],
  },
  bat: <MonsterInfo>{
    id: 'bat',
    name: 'a giant bat',
    level: 0,
    damage: 1,
    health: 1,
    onDamageDoneText: [],
    onDamageTakenText: [],
  },
  monster_rat: <MonsterInfo>{
    id: 'monster_rat',
    name: 'a monster rat',
    level: 1,
    damage: 1,
    health: 2,
    onDamageDoneText: [],
    onDamageTakenText: [],
  },
  goblin: <MonsterInfo>{
    id: 'goblin',
    name: 'a goblin',
    level: 1,
    damage: 1,
    health: 2,
    onDamageDoneText: [],
    onDamageTakenText: [],
  }
}