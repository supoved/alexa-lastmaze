import { Item, Weapon, UsableItem } from "./types";

export const healItems: UsableItem[] = [
  {
    id: 'heal-herbs',
    name: 'restoration herbs',
    description: 'Can be used to treat wounds, when not in a combat',
    canBeUsedInBattle: false,
    type: "Healing",
    level: 0
  }
];

export const utilities: UsableItem[] = [
  {
    id: 'escape-dust',
    name: 'a dust of escape',
    description: 'Creates a dust cloud to help you retreat, if battle have not started yet',
    canBeUsedInBattle: false,
    type: "Utility",
    level: 1
  }
];

export const treasure: Item[] = [
  {
    id: 'coin',
    name: 'a golden coin',
    description: 'Shiny coin',
    type: 'Treasure',
    level: 1
  }
];

export const weapons: Weapon[] = [
  {
    id: 'dagger',
    name: 'a dagger',
    description: 'A trustly companion, that can help to defend your self, if you have nothing better.',
    canBeUsedInBattle: false,
    type: 'Weapon',
    damage: 1,
    level: -1
  },
  {
    id: 'throwing-knife',
    name: 'a throwing knife',
    description: 'Can be used before battle begins. By default 30% chance to hit enemy',
    canBeUsedInBattle: false,
    type: 'Weapon',
    damage: 1,
    level: 1
  },
  {
    id: 'old-sword',
    name: 'an old sword',
    description: 'This sword is rather old, but still good against small monsters',
    canBeUsedInBattle: false,
    type: 'Weapon',
    damage: 2,
    level: 2
  }
];