import { Container, ContainerInfo } from "./types";

export const containers: ContainerInfo[] = [
  {
    id: "bag",
    type: "Chest",
    name: 'a traveler bag',
    description: 'A bag of some unlucky traveler',
    health: 1,
    lockLevel: 0,
    lootLevel: 1
  },
  {
    id: "wooden-box",
    type: "Chest",
    name: 'an old box',
    description: 'Old box made of wood',
    health: 2,
    lockLevel: 1,
    lootLevel: 1
  },
  {
    id: "wooden-box",
    type: "Chest",
    name: 'an old wooden chest',
    description: 'Old wooden chest, doesnt look strong or valuable',
    health: 3,
    lockLevel: 2,
    lootLevel: 2
  }
]