import { MazeState } from "./core/types";
import { combine } from '../lib/tools/words';

export function isAlive(maze: MazeState): boolean {
  console.log(`isAlive: ${maze.player.currentHealth}`)
  return maze.player.currentHealth > 0;
}

export function endGameScore(maze: MazeState): string {
  return combine(
    `You have passed ${maze.stats.roomsPassed} rooms`,
    `Defeated ${maze.stats.monstersDefeated} monsters`,
    `And collected ${maze.stats.goldCollected} gold`
  );
}
