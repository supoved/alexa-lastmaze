import { Room, DoorContent, DoorType, Door, ExitType, Container } from './core/types'
import { pickRandomItem, removeRandomItem } from './tools/random'
import { containers } from './core/roomContent';
import { treasure } from './core/items';
import { keys, assign, isEmpty } from 'lodash';
import { generateMonster } from './fight';
import * as moment from 'moment';
import { getRandomMessage, combine } from './tools/words';


export function pickDoor(currentRoom: Room, doorId: string): Door {
  console.log(`pickDoor: ${JSON.stringify(currentRoom.exits)} with ${doorId}`);
  return currentRoom.exits[doorId];
}

export function describeRoom(room: Room): string {
  let roomInfo = room.description;
  let monserInfo = '';
  let contentInfo = '';
  let doorsInfo = `There are ${keys(room.exits).length} doors.`;

  if (room.monsters.length > 0) {
    monserInfo = getRandomMessage('room_monster', 1, { monster: room.monsters[0].name });
    doorsInfo = '';
  }
  
  if (room.content) {
    if(room.content.type == 'Chest'){
      if ((room.content as Container).items.length > 0){
        contentInfo = getRandomMessage('room_item', 1, { item: room.content.name })
      }
    }  else {
      contentInfo = getRandomMessage('room_item', 1, { item: room.content.name })
    }
  }

  return combine(roomInfo, monserInfo, contentInfo, doorsInfo);
}

export function generateBehindDoorRoom(lastRoomId: number, door: Door): Room {
  console.log('generateBehindDoorRoom: ' + JSON.stringify(door));
  let newRoom = generateRoom({
    content: door.content,
    type: door.type,
    roomId: lastRoomId
  }, 3, ["left", "center", "right"], ["Regular", "Loot", "Danger"], ["Door", "Door", "Door"]);
  return newRoom;
}

export function generateStartRoom(): Room {
  console.log('generateStartRoom');
  let entrance: Door = {
    content: "Regular",
    type: "OneWayDoor"
  }

  return generateRoom(entrance, 3, ["left", "center", "right"], ["Regular", "Loot", "Danger"], ["Door", "Door", "Door"]);
}

export function generateRoom(entrance: Door, doorsCount: number, exitOptions: ExitType[], contentOptions: DoorContent[], doorsOptions: DoorType[]) {
  console.log('generateRoom');
  let room: Room = {
    id: moment.utc().valueOf(),
    exits: {},
    entrance,
    content: null,
    monsters: [],
    description: getRandomMessage('room_view', 0.4)
  }

  for (var i = 0; i < doorsCount; i++) {
    room.exits[removeRandomItem(exitOptions)] = {
      type: removeRandomItem(doorsOptions),
      content: removeRandomItem(contentOptions)
    };
  }

  if (entrance.content === 'Loot') {
    let container: Container = assign(pickRandomItem(containers), {
      items: [treasure[0]],
      locked: false
    });

    room.content = container;
  } else if (entrance.content === 'Danger') {
    room.monsters.push(generateMonster(1));
  }

  return room;
}
