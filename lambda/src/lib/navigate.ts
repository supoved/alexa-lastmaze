import { MazeState, Room } from "./core/types";
import { find, chain, isEmpty, map, keys } from 'lodash';
import { pickDoor, describeRoom, generateBehindDoorRoom } from './rooms';
import { getRandomMessage, combine } from './tools/words';
import { isSuccess } from "./tools/random";


export interface NavigationResult {
  success: boolean,
  message: string
}

export function navigate(maze: MazeState, direction: string): NavigationResult {
  console.log(`room: ${maze.room.id}  navigate: ${direction}`);
  if (direction == "back") {
    console.log('navigate: returning back');
    if (maze.room.entrance.type == "OneWayDoor") {
      return {
        success: false,
        message: getRandomMessage('path_sealed')
      };
    }
    else {
      let currentRoom = maze.room;
      maze.knownRooms.push(maze.room);
      maze.room = getRoomFromHistory(maze.knownRooms, currentRoom.entrance.roomId);

      if (currentRoom.monsters.length > 0) {
        console.log('navigate: monster present');
        let escaped = isSuccess(0.6);
        if (!escaped) {
          console.log('navigate: monster hit player');
          maze.player.currentHealth--;
        }

        return {
          success: true,
          message: escaped ? "You have escaped to the last room." : "You have been hit by the moster and barely escaped to the last room."
        }
      }

      return {
        success: true,
        message: "You have returned to the last room."
      }
    }
  }

  let door = pickDoor(maze.room, direction);
  if (!door) {
    return {
      success: false,
      message: getRandomMessage('path_noway')
    };
  }

  if (maze.room.monsters.length > 0) {
    return {
      success: false,
      message: getRandomMessage('path_monster', 0, { monster: maze.room.monsters[0].name })
    };
  }

  let knownRoom = door.roomId ? getRoomFromHistory(maze.knownRooms, door.roomId) : undefined;
  if (knownRoom) {
    console.log('navigate: moving to known room');
    maze.room = knownRoom;
    return {
      success: true,
      message: combine(
        getRandomMessage('path_known'),
        describeRoom(maze.room)
      )
    }
  } else {
    console.log('navigate: moving to a new unkwnown room');
    let currentRoom = maze.room;
    let newRoom = generateBehindDoorRoom(currentRoom.id, door);
    door.roomId = newRoom.id;
    currentRoom.entrance.type = "OneWayDoor";

    maze.knownRooms = refreshRoomsHistory(maze.knownRooms, currentRoom);
    maze.room = newRoom;

    if (maze.room.entrance.type != 'OneWayDoor') {
      maze.stats.roomsPassed++;
    }

    return {
      success: true,
      message: combine(
        getRandomMessage('path_new'),
        describeRoom(maze.room)
      )
    }
  }
}

function refreshRoomsHistory(currentHistory: Room[], lastRoom: Room): Room[] {
  let freshHistory = [lastRoom];
  let knownRoomIds: number[] = chain(keys(lastRoom.exits)).map((key) => { return lastRoom.exits[key].roomId }).without(undefined).value();
  currentHistory.forEach(room => {
    if (knownRoomIds.indexOf(room.id) >= 0) {
      freshHistory.push(room)
    }
  });
  return freshHistory;
}

function getRoomFromHistory(currentHistory: Room[], roomId: number): Room {
  console.log(`find room with id: ${roomId} in ${JSON.stringify(map(currentHistory, 'id'))}`);
  return find(currentHistory, { id: roomId });
}