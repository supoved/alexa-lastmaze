import { RequestHandler } from 'ask-sdk-core';

export const HelpIntentHandler: RequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'To naviogate around say: "go left. To open boxes say: "open". To fight monsters say: "scissors, rock or paper". To runaway, say: "Run away"';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Help', speechText)
      //.withShouldEndSession(false)
      .getResponse();
  },
};