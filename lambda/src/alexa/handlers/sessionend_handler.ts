import { RequestHandler } from 'ask-sdk-core';
import { SessionEndedRequest } from 'ask-sdk-model';

export const SessionEndedRequestHandler: RequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${(<SessionEndedRequest>handlerInput.requestEnvelope.request).reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};