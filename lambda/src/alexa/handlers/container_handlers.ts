import { RequestHandler } from 'ask-sdk-core';
import { MazeState, Container } from '../../lib/core/types'
import { IntentRequest } from 'ask-sdk-model';
import { isEmpty } from 'lodash';
import { isAlive } from '../../lib/game';
import { combine, getRandomMessage } from '../../lib/tools/words';
import { describeRoom } from '../../lib/rooms';

export const OpenContainerHandler: RequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'OpenContainerIntent'
      && isAlive(handlerInput.attributesManager.getSessionAttributes().mazeState);
  },
  handle(handlerInput) {
    let intentRequest = handlerInput.requestEnvelope.request as IntentRequest;
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    let mazeState: MazeState = attributes.mazeState;
    let speechText = '';


    if (!mazeState.room.content || mazeState.room.content.type !== 'Chest') {
      speechText = 'You cant find any chest to open';
    } else {
      let chest = mazeState.room.content as Container;
      let contentDescription = isEmpty(chest.items) ? 'nothing but dust and spiders' : chest.items[0].name;
      speechText = `You have opened ${chest.name}. And found ${contentDescription}.`
      
      chest.items.forEach(item => {
        if(item.type == "Treasure"){
          mazeState.stats.goldCollected += item.level;
        }
        //move items to backpack
      });
      chest.items = [];
    }

    handlerInput.attributesManager.setSessionAttributes(attributes);

    return handlerInput.responseBuilder
      //.withShouldEndSession(false)
      .speak(combine(speechText, describeRoom(mazeState.room), getRandomMessage('path_choose', 0.3)))      
      .reprompt(combine(describeRoom(mazeState.room), getRandomMessage('path_choose')))
      .getResponse();
  }
};