import { RequestHandler } from 'ask-sdk-core';
import { MazeState } from '../../lib/core/types'
import { generateStartRoom } from '../../lib/rooms'
import { weapons } from '../../lib/core/items'

export const LaunchRequestHandler: RequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest'
  },
  handle(handlerInput) {
    const speechText = 'Welcome to the Dungeon! You are in an empty room, covered with dust and the entrance is now sealed behind you. There are three passeges in front of you. Which one you want to go?';
    const repromptText = 'There are three passeges in front of you. Which one you want to go? Like: go left';

    let attributes = handlerInput.attributesManager.getSessionAttributes();
    let mazeState: MazeState = {
      player: {
        currentHealth: 3,
        maxHealth: 3,
        isAlive: true,
        inventory: [],
        weapon: weapons[0]        
      },
      knownRooms: [],
      room: generateStartRoom(),
      stats: { monstersDefeated: 0, roomsPassed: 0, goldCollected: 0 }
    };
    attributes.mazeState = mazeState;
    handlerInput.attributesManager.setSessionAttributes(attributes);

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(repromptText)
      .getResponse();
  },
};