import { ErrorHandler } from 'ask-sdk-core';
import { isAlive, endGameScore } from '../../lib/game';

export const UnknownErrorHandler: ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);
    let intent = handlerInput.requestEnvelope.request.type === 'IntentRequest' ? handlerInput.requestEnvelope.request.intent.name : '';
    let maze = handlerInput.attributesManager.getSessionAttributes().mazeState;
    let alive = isAlive(maze);
    if (!alive) {
      return handlerInput.responseBuilder
        .withShouldEndSession(true)
        .speak(`It is sad, but you have died. Here is your score. ` + endGameScore(maze))
        .getResponse();
    }

    return handlerInput.responseBuilder
      .withShouldEndSession(false)
      .speak(`Sorry. Please say again or say 'help'`)
      .getResponse();
  },
};