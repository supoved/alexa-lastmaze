import { RequestHandler } from 'ask-sdk-core';
import { MazeState } from '../../lib/core/types'
import { IntentRequest } from 'ask-sdk-model';
import { getSlotInfo } from '../utils';
import { navigate } from '../../lib/navigate';
import { map } from 'lodash';
import { isAlive, endGameScore } from '../../lib/game';
import { describeRoom } from '../../lib/rooms';
import { combine, getRandomMessage } from '../../lib/tools/words';

export const NavigateRequestHandler: RequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'NavigateIntent'
      && isAlive(handlerInput.attributesManager.getSessionAttributes().mazeState);
  },
  handle(handlerInput) {
    let intentRequest = handlerInput.requestEnvelope.request as IntentRequest;
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    let mazeState: MazeState = attributes.mazeState;

    let slotInfo = getSlotInfo(intentRequest.intent.slots.direction);

    let result = navigate(mazeState, slotInfo.value);
    if (result.success) {
      console.log(`entered new room: ${JSON.stringify(mazeState.room)}`)
      console.log(`rooms in history: ${JSON.stringify(map(mazeState.knownRooms, 'id'))}`)
    }
    handlerInput.attributesManager.setSessionAttributes(attributes);

    if (!isAlive(mazeState)) {
      return handlerInput.responseBuilder
        .withShouldEndSession(true)
        .speak(`It is sad, but you failed to escape and died. Here is your score. ` + endGameScore(mazeState))
        .getResponse();
    }

    return handlerInput.responseBuilder
      //.withShouldEndSession(false)
      .speak(result.message)
      .reprompt(combine(describeRoom(mazeState.room), getRandomMessage('path_choose')))
      .getResponse();
  }
};