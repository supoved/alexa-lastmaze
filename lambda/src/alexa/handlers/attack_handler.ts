import { RequestHandler } from 'ask-sdk-core';
import { MazeState, AttackType } from '../../lib/core/types'
import { attackMonster } from '../../lib/fight'
import { IntentRequest } from 'ask-sdk-model';
import { getSlotInfo } from '../utils';
import { isAlive, endGameScore } from '../../lib/game';
import { combine, getRandomMessage } from '../../lib/tools/words'
import { describeRoom } from '../../lib/rooms';

export const AttackRequestHandler: RequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AttackIntent'
      && isAlive(handlerInput.attributesManager.getSessionAttributes().mazeState);
  },
  handle(handlerInput) {
    let intentRequest = handlerInput.requestEnvelope.request as IntentRequest;
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    let mazeState: MazeState = attributes.mazeState;

    let speechText = '';
    let repromt = '';
    if (!mazeState.room.monsters[0]) {
      speechText = 'Room is empty, no one to attack';
      repromt = combine(describeRoom(mazeState.room), getRandomMessage('path_choose'));
    } else {
      let slotInfo = getSlotInfo(intentRequest.intent.slots.attack);
      let result = attackMonster(mazeState.player, mazeState.room.monsters[0], slotInfo.id as AttackType);

      console.log('Player: ' + JSON.stringify(mazeState.player));
      console.log('Monster: ' + JSON.stringify(mazeState.room.monsters[0]));
      console.log('Attack: ' + JSON.stringify(result));

      if (!mazeState.player.isAlive) {
        //finish game      
        return handlerInput.responseBuilder
          .speak(`It is sad, but you have died. Here is your score. ` + endGameScore(mazeState))
          .withShouldEndSession(true)
          .getResponse();
      }

      if (!mazeState.room.monsters[0].isAlive) {
        // support multiple of them
        mazeState.room.monsters.pop();
        mazeState.stats.monstersDefeated++;

        speechText = combine(result.message, describeRoom(mazeState.room));
        repromt = combine(describeRoom(mazeState.room), getRandomMessage('path_choose'));
      } else {
        speechText = combine(result.message, getRandomMessage('attack_next'));
        repromt = combine(getRandomMessage('room_monster'), getRandomMessage('atack_choose'))
      }

      handlerInput.attributesManager.setSessionAttributes(attributes);
    }

    return handlerInput.responseBuilder
      .speak(speechText)
      //.withShouldEndSession(false)
      .reprompt(repromt)      
      .getResponse();
  }
};
