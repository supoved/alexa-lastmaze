import { RequestHandler } from 'ask-sdk-core';
import { endGameScore } from '../../lib/game';

export const StopIntentHandler: RequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    let intent = handlerInput.requestEnvelope.request.type === 'IntentRequest' ? handlerInput.requestEnvelope.request.intent.name : '';
    let maze = handlerInput.attributesManager.getSessionAttributes().mazeState;
    const speechText = 'Goodbye! Here is your score. ' + endGameScore(maze);

    return handlerInput.responseBuilder
      .speak(speechText)
      .getResponse();
  },
};