import { Slot, slu } from "ask-sdk-model";

export interface ISlotInfo {
  name: string,
  raw: string,
  value: string,
  id: string
}

export function getSlotInfo(slot: Slot): ISlotInfo {
  console.log(`getSlotInfo: ${JSON.stringify(slot)}`)

  let resolution = getSuccesfulResolutionValue(slot.resolutions.resolutionsPerAuthority);
  console.log(`ResolutionValue: ${JSON.stringify(resolution)}`)

  return {
    name: slot.name,
    raw: slot.value,
    value: resolution.name,
    id: resolution.id
  };
}

function getSuccesfulResolutionValue(resolutions: Array<slu.entityresolution.Resolution>): slu.entityresolution.Value {
  let result = { name: undefined, id: undefined };

  resolutions.forEach(element => {
    console.log(`getSuccesfulResolutionValue: ${JSON.stringify(element)}`)
    if (element.status.code == "ER_SUCCESS_MATCH") {
      console.log(`getSuccesfulResolutionValue: SUCCESS ${JSON.stringify(element.values[0].value)}`)
      result = element.values[0].value;
    }
  });
  return result;
}