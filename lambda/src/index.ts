import * as Alexa from 'ask-sdk-core';

import { LaunchRequestHandler } from './alexa/handlers/launch_handler';
import { HelpIntentHandler } from './alexa/handlers/help_handler';
import { StopIntentHandler } from './alexa/handlers/stop_handler';
import { SessionEndedRequestHandler } from './alexa/handlers/sessionend_handler';
import { UnknownErrorHandler } from './alexa/handlers/error_handler';
import { NavigateRequestHandler } from './alexa/handlers/navigate_handler';
import { OpenContainerHandler } from './alexa/handlers/container_handlers';
import { AttackRequestHandler } from './alexa/handlers/attack_handler';

const skillBuilder = Alexa.SkillBuilders.custom();
export const handler = skillBuilder
  .addRequestHandlers(
    AttackRequestHandler,    
    NavigateRequestHandler,
    OpenContainerHandler,
    LaunchRequestHandler,
    HelpIntentHandler,
    StopIntentHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(UnknownErrorHandler)
  .lambda();
